# 3tcd
3tcd is an acronym for terraform, terragrunt, tflint continuous deployment.

The image is an all in one image for cloud Deployments.
Depending on the use case it can be used as a base image for a CI/CD pipeline or as a standalone image for local development.
For CI/CD Pipelines, it can be better to use more specific images for each tool.

## Image content of 3tcd
- terraform
- terragrunt
- tflint
- git
- bash
- skopeo
- aws-cli
- nerdctl
