# Gitlab CI-CD Templates Terraform AWS

This repository contains Gitlab CI-CD templates for building containers and deploying Terraform and Terragrunt Code to AWS.
We try not to reinvent the wheel and use the "best" practices from the community.
If required, the available templates are extended with our own experiences and ideas.

## Getting started
Load the required template into your Gitlab CI-CD pipeline.
Configure the template with your specific settings.
Enjoy the automation.

## Templates
### Build Containers with buildah
We use [buildah](https://buildah.io/) to build container images. The buildah tool is a part of the [podman](https://podman.io/) project.
This template extends the [GitLab CI template for Docker](https://gitlab.com/explore/catalog/to-be-continuous/docker) from [to be coninuous](https://gitlab.com/explore/catalog/to-be-continuous/docker)
in a way to build multiarch images with buildah in parallel on native arch runners.
The images are then merged into a manifest list by buildah manifest tool.

template file: [build-container.gitlab-ci.yml](templates/build-container-buildah.gitlab-ci.yml)

Load the Gitlab CI template for building a container image with buildah.
```yaml
include:
  # replace main with a specific tag, e.g. v0.1.0
  - remote: 'https://gitlab.com/csautter/gitlab-ci-cd-templates/-/raw/main/templates/build-container.gitlab-ci.yml'
#   loading as component is not working in all environments (like on prem gitlab)
#  - project: 'gitlab.com/csautter/gitlab-ci-cd-templates/'
#    file: 'templates/build-container-buildah.gitlab-ci.yml'
#    ref: 'main' # or a specific tag, e.g. v0.1.0
```
#### Advantages of our multiarch build approach
- Build multiarch images in parallel on native arch runners
- No emulation required if you build containers with compile steps
- at least 2x faster than building on a single runner
- can increase compile speed up to factor 4

#### Disadvantages of our multiarch build approach
- Requires multiple runners
- You should bring your own runners for different architectures (e.g. arm64, amd64)
- Can increase complexity of the pipeline

#### Configuration
__Important if you need to build multiarch images:__
If you need to compile for a different architecture, 
you can enable qemu emulation by setting the environment variable 
`BUILDAH_ENABLE_QEMU_EMULATION` to `true`.
If you are just building containers with pre compiled binaries,
you should keep qemu emulation disabled because it's not required and much slower.

__Note:__ You should always prefer to build images on a runner with the native architecture if possible.
```yaml
stages:
  - build
  - test
  - package-build
  - package-manifest
  - package-test
  - infra
  - deploy
  - acceptance
  - publish
  - infra-prod
  - production

variables:
  DOCKER_PROD_PUBLISH_STRATEGY: 'manual'
  DOCKER_BUILD_CACHE_DISABLED: 'false'
  # BUILDAH_ENABLE_QEMU_EMULATION: 'true' # optional, if you need to compile for a different architecture

.matrix-parallel-multi-arch:
  parallel:
    matrix:
      - TARGETARCH: [ "amd64", "arm64" ]
        DOCKER_FILE: 'build/docker/3tcd/Dockerfile'
        DOCKER_IMAGE_BASE_NAME: '3tcd'
      # - TARGETARCH: [ "amd64", "arm64" ]
      #   DOCKER_FILE: 'build/docker/terraform/Dockerfile'
      #   DOCKER_IMAGE_BASE_NAME: 'terraform'
      #   BUILDAH_ENABLE_QEMU_EMULATION: 'true' # enable qemu emulation for non-native architectures, only required for building packages from src

.matrix-single-multi-arch:
  parallel:
    matrix:
      - TARGETARCHS: "amd64 arm64"
        DOCKER_IMAGE_BASE_NAME: '3tcd'
      # - TARGETARCHS: "amd64 arm64"
      #   DOCKER_IMAGE_BASE_NAME: 'terraform'
```
#### Buildah related links
- [buildah.io](https://buildah.io/)
- [podman.io](https://podman.io/)
- [Best practices for running Buildah in a container](https://developers.redhat.com/blog/2019/08/14/best-practices-for-running-buildah-in-a-container#)

### Update dependencies with renovate
Load the renovate template
```yaml
include:
  - project: 'to-be-continuous/renovate'
    ref: '1.2.1'
    file: '/templates/gitlab-ci-renovate.yml'
  # replace main with a specific tag, e.g. v0.1.0
  - remote: 'https://gitlab.com/csautter/gitlab-ci-cd-templates/-/raw/main/templates/renovate.gitlab-ci.yml'
```

#### Generate a project access token for renovate
To use the renovate bot, you need to generate a project access token.
- Go to your Gitlab project settings
- Navigate to `Settings` -> `Access Tokens`
- Create a new access token with the following permissions:
  - `api`
  - `read_registry`
  - `write_repository`
- Navigate to `CI/CD` -> `Variables`
- Add a new variable with the name `RENOVATE_TOKEN` and the value of the generated access token
- Set the variable to `protected` if you want to restrict access to the token
- Set the variable to `masked` if you want to hide the token in the logs

#### Configuration
Have a look at the project config: [renovate.json](.gitlab/renovate.json)

#### Renovate related links
- [Renovate Docs](https://docs.renovatebot.com/)
- [Renovate Configuration Options](https://docs.renovatebot.com/configuration-options/)
- https://to-be-continuous.gitlab.io/doc/ref/renovate/
- https://gitlab.com/to-be-continuous/renovate

## License
This project is licensed under the GNU GENERAL PUBLIC LICENSE Version 3 - see the [LICENSE](LICENSE) file for details.

## Project status
This project is in early development. There are no releases yet. I do not recommend using it in production.
